1. Install Genymotion
1. Add a new virtual device (I used the "Custom Phone - 5.1.0 - API 22" image)
1. Start the virtual device
1. Once it's booted up, drag and drop "Genymotion-ARM-Translation_v1.1.zip" into the VM. If prompted to flash the device, do so.
1. Reboot the virtual device ("adb reboot" works best)
1. Once it's booted up again, drag and drop "gapps-kk-20140606.zip" into the VM. If prompted to flash the device, do so.
1. Reboot the virtual device.
1. Start the Play Store app
1. Update the Google+ app via the Play Store (to avoid some nagging errors)
1. Install the Facebook app via the Play Store
1. You should be able to run the Facebook app within the VM now.